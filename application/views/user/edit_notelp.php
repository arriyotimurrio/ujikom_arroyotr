<div class="container-fluid">

    <!-- Page Heading -->
    <a href="<?= base_url('User/ProfileController') ?>" class="btn btn-danger"><i class="fas fa-arrow-left"></i></a>
    <h1></h1>
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <?= validation_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">', '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>') ?>
    <?= $this->session->flashdata('msg'); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= form_open('User/ProfileController/'); ?>
            <div class="form-group">
                <label for="telp">Masukan Nomor Telp Baru</label>
                <input type="text" class="form-control" id="telp" placeholder="" name="telp" value="<?= $user['telp'] ?>">
            </div>
            <div>
            </div>
            <button type="submit" class="btn btn-warning">Submit</button>
            <?= form_close(); ?>
        </div>
    </div>

    <!-- /.container-fluid -->
</div>