<!DOCTYPE html>
<html>


<body>
    <style type="text/css">
        body {
            font-family: sans-serif;
        }

        table {
            margin: 20px auto;
            border-collapse: collapse;
        }

        table th,
        table td {
            border: 1px solid #3c3c3c;
            padding: 3px 8px;

        }

        a {
            background: blue;
            color: #fff;
            padding: 8px 10px;
            text-decoration: none;
            border-radius: 2px;
        }
    </style>

    <?php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Laporan Pengaduan.xls");
    ?>

    <table class="table">
			<thead class="thead-dark">
				<tr>
					<th scope="col">No</th>
					<th scope="col">Nama</th>
					<th scope="col">Nik</th>
					<th scope="col">Laporan</th>
					<th scope="col">Tgl P</th>
					<th scope="col">Status</th>
					<th scope="col">Tanggapan</th>
					<th scope="col">Tgl T</th>
				</tr>
			</thead>
			<tbody>

				<?php $no = 1; ?>
				<?php foreach ($laporan as $l) : ?>
					<tr>
						<th scope="row"><?= $no++; ?></th>
						<td><?= $l['nama'] ?></td>
						<td><?= $l['nik'] ?></td>
						<td><?= $l['isi_laporan'] ?></td>
						<td><?= $l['tgl_pengaduan'] ?></td>
						<td>
							<?php
							if ($l['status'] == '0') {

							?>
								<span class="badge badge-secondary">Sedang di verifikasi</span>
							<?php } elseif ($l['status'] == 'proses') { ?>
								<span class="badge badge-primary">Sedang di proses</span>
							<?php } elseif ($l['status'] == 'selesai') { ?>
								<span class="badge badge-success">Selesai di kerjakan</span>
							<?php } elseif ($l['status'] == 'tolak') { ?>
								<span class="badge badge-danger">Pengaduan di tolak</span>
							<?php }
							?>
						</td>
						<td><?= $l['tanggapan'] == null ? '-' : $l['tanggapan']; ?></td>
						<td><?= $l['tgl_tanggapan'] == null ? '-' : $l['tgl_tanggapan']; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
</body>

</html>
