-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Apr 2022 pada 06.20
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_arroyo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `masyarakat`
--

CREATE TABLE `masyarakat` (
  `nik` bigint(16) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(225) NOT NULL,
  `password2` varchar(255) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `foto_profile` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `masyarakat`
--

INSERT INTO `masyarakat` (`nik`, `nama`, `username`, `password`, `password2`, `telp`, `foto_profile`) VALUES
(113020, 'Zizi', 'zee', '$2y$10$w9hphvDZEGCzmLU.cv8n3O5S/Abxuo9fkIJWRXhT2j0wHvbvgXoiG', '', '081918980989', 'user.png'),
(113022, 'Marsha', 'matcha', '$2y$10$86XMAYPB8F/sC15F/wU60uiWy3TnDV1poAx32.5eObC.Nol9Iruxm', '', '0813234786', 'user.png'),
(3346878132411313, 'Daffa Thandika', 'daffata', '$2y$10$3c3KMFCgg9Kxy7eR6e8Es.XlGxwsonmbnggM3x8o2PG5wg.drL8im', '', '081325224252', 'user.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_pengaduan` bigint(16) NOT NULL,
  `tgl_pengaduan` date NOT NULL,
  `nik` bigint(16) NOT NULL,
  `isi_laporan` text NOT NULL,
  `foto` varchar(255) NOT NULL,
  `status` enum('0','proses','selesai','tolak') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengaduan`
--

INSERT INTO `pengaduan` (`id_pengaduan`, `tgl_pengaduan`, `nik`, `isi_laporan`, `foto`, `status`) VALUES
(9, '2022-02-03', 134145, 'gogogo', '77148cdb8657b57733c62d2578a224de.jpg', '0'),
(11, '2022-03-11', 113022, 'ily', 'b90b860deb91e08a3036d2e1f5f1a33c.jpg', 'selesai'),
(12, '2022-03-11', 113022, 'ok', '91bad43bf9711e6cddde1da2d7ba8ee3.jpg', 'proses'),
(13, '2022-03-11', 21334, 'redfgydefxv f', '4b08dca5f434ebe6a1a7b4c4e45e8a96.jpg', 'selesai'),
(14, '2022-03-25', 113022, 'ada koala', 'c596af49b70261183b9e1aeae38e793a.jpg', 'selesai'),
(15, '2022-04-14', 113022, 'ada pinguin', '1d2f2811837c955211d8c7716e7b9348.jpg', 'tolak'),
(16, '2022-04-14', 3346878132411313, 'Air menggenang di sekitaran blok B, jalan Permata, kendaraan warga masih bisa berlalu-lalang namun mohon untuk segera di perbaiki.', '1dd7a6a2486bd165609658504f6e72bf.jpg', 'proses');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(35) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(225) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `level` enum('admin','petugas') NOT NULL,
  `foto_profile` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama_petugas`, `username`, `password`, `telp`, `level`, `foto_profile`) VALUES
(8, 'Arroyo Timur Rio', 'arroyo', '$2y$10$QAl1sJ6G.zcV1wRms2zPQuxqpjcCKeG1.npjwskUDirAk7F3SQSfC', '081254367468', 'admin', 'user.png'),
(9, 'Rio', 'rio', '$2y$10$KHcNlAbhJmnQpeMksFwHjenJIlz16M0JQnl4McfYTJJIVEfRqiUgy', '081723984389', 'petugas', 'user.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanggapan`
--

CREATE TABLE `tanggapan` (
  `id_tanggapan` int(11) NOT NULL,
  `id_pengaduan` bigint(16) NOT NULL,
  `tgl_tanggapan` date NOT NULL,
  `tanggapan` text NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tanggapan`
--

INSERT INTO `tanggapan` (`id_tanggapan`, `id_pengaduan`, `tgl_tanggapan`, `tanggapan`, `id_petugas`) VALUES
(1, 11, '2022-03-11', 'ilyt', 2),
(2, 12, '2022-03-11', 'ok', 2),
(3, 13, '2022-03-11', 'dgvsdfgs', 2),
(4, 14, '2022-03-25', 'ok', 8),
(5, 15, '2022-04-14', 'di tolak', 8),
(6, 16, '2022-04-14', 'Terima kasih atas laporan Bapak, mohon untuk menunggu sebentar akan segera kami tangani', 8);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `masyarakat`
--
ALTER TABLE `masyarakat`
  ADD PRIMARY KEY (`nik`);

--
-- Indeks untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`),
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `tanggapan`
--
ALTER TABLE `tanggapan`
  ADD PRIMARY KEY (`id_tanggapan`),
  ADD KEY `id_pengaduan` (`id_pengaduan`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pengaduan`
--
ALTER TABLE `pengaduan`
  MODIFY `id_pengaduan` bigint(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tanggapan`
--
ALTER TABLE `tanggapan`
  MODIFY `id_tanggapan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
